# README #

## Tasks
 - use real api as offer source http://www.qarson.fr.rc.e-d-p.net/api/offers_list/175265
 - our client can't see full source api response
   - keep xml response format (for current aps)
   - full list collected from paginated response http://www.qarson.fr.rc.e-d-p.net/api/offers_list/0?page=10
 - add car image to results - relative path for xml, full path (show image) in html
 - discuss / plan
   - how to filter full list of cars
   - how to add to each car status information (available from different api) with possibility to filter by this information
